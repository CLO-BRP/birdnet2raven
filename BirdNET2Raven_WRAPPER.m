% script BirdNET2Raven_WRAPPER.m
%   Calls BirdNET2Raven for each bird species

% TEMP %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % clear
% % clc
disp('*** TO DO ***************************************************************')
disp('0. Report DOS errors to console, pausing execution.')
disp('1. Export xlsx with number of clips, num selections, ...')
disp('   min/max lat, min/max lon, min/max alt, North America/Europe/other')
disp('2. Export ST for all species. Tell Dan & Holger.')
disp('3. Browse with strict TP criterion, per Holger.')
disp('2. Write script for plotting Recall x score bin, and precision x recall')
disp('*************************************************************************')
disp(' ')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% User input
inpath = 'T:\projects\2018_CLO_IthacaNY_S1084\BirdNET-testing\BirdNET_clips';
outpath = 'T:\projects\2018_CLO_IthacaNY_S1084\BirdNET-testing\Analysis\selection_tables';

% Find directory of bird species folders
option = '/b/ad | sort';
[status, result] = dos(sprintf('dir "%s" %s', inpath, option));
if status || strcmp(result,sprintf('File Not Found\n'))
    d = {};
    return;
end
species = strsplit(result, newline)';
species(strcmp('', species)) = [];
p = fullfile(inpath, species);

%Export BirdNET meta data as Raven selection tables

for i = 1:length(species)
    currSpecies = species{i};
    currInpath = p{i};
    currOutpath = fullfile(outpath,currSpecies);
    assert(~isfolder(currOutpath), ...
        sprintf('\nOutput folder already exists:\n%s\n',currOutpath))
    mkdir(outpath,species{i})
    BirdNET2Raven(currSpecies,currInpath,currOutpath)
end

disp(' ')
disp('***** Processing complete *****')