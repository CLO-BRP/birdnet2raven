function BirdNET2Raven(label,inpath,outpath)
%   Makes selection table for browsing BirdNET events in Raven Pro

% % % User input
% % label = 'BlueJay';
% % inpath = 'T:\projects\2018_CLO_IthacaNY_S1084\BirdNET-testing\Blue Jay\BirdNET_clips';
% % outpath = 'T:\projects\2018_CLO_IthacaNY_S1084\sandbox2_msp2';

% Set paths
fnST = sprintf('%s.selections.txt',label);
fnScores = sprintf('%s.scores.txt',label);
fnListfile = sprintf('%s.listfile.txt',label);
fnSTfull = fullfile(outpath,fnST);
fnScoresFull = fullfile(outpath,fnScores);
fnListfileFull = fullfile(outpath,fnListfile);
assert(~isfile(fnSTfull),'ERROR: Selection table output already exists.')
assert(~isfile(fnScoresFull),'ERROR: Scores table output already exists.')
assert(~isfile(fnListfileFull),'ERROR: Listfile output already exists.')

% Find contents of input directory
fnMeta = fastDir(inpath,true,'json');
fnSound = fastDir(inpath,true,'wav');

% Read BirdNET metadata files
numClips = length(fnMeta);
score = cell(numClips,1);
species = cell(numClips,1);
% % recordingId = cell(numClips,1);
% % appVersion = cell(numClips,1);
% % ts = cell(numClips,1);
% % deviceId = cell(numClips,1);
lat = cell(numClips,1);
lon = cell(numClips,1);
alt = cell(numClips,1);
for i = 1:numClips
    fid = fopen(fnMeta{i}); 
    raw = fread(fid,inf); 
    str = char(raw'); 
    fclose(fid);
    data = jsondecode(str);
    prediction = struct2cell(orderfields(data.prediction));
    speciesCurr = cellfun(@(x) x.species,prediction,'Unif',0);
    scoreCurr = str2double(cellfun(@(x) x.score,prediction,'Unif',0));
    [scoreCurr,idx] = max(scoreCurr);
    score(i) = cellstr(num2str(scoreCurr,11));
    species(i) = speciesCurr(idx);
% %     recordingId(i) = {data.meta.recordingId};
% %     appVersion(i) = {data.meta.appVersion};
% %     ts(i) = {data.meta.ts};
    lat(i) = {data.meta.gps.lat};
    lon(i) = {data.meta.gps.lon};
    alt(i) = {data.meta.gps.alt};
% %     deviceId(i) = {data.meta.deviceId};
    
end

% Read headers of BirdNET sound clips
info = cellfun(@audioinfo,fnSound);
bits = [info.BitsPerSample]';
assert(all(bits==bits(1)),'ERROR: All sound clips must have same bit depth.')
Fs = [info.SampleRate]';
assert(all(Fs==Fs(1)),'ERROR: All sound clips must have same sample rate.')
dur = [info.Duration]';
cumDur = cumsum(dur);
beginTime = cellstr(num2str([0;cumDur(1:end-1)]));
endTime = cellstr(num2str(cumDur));
chan = [info.NumChannels]';
assert(all(chan==1),'ERROR: All sound clips must have one channel.')

% Set up variables for selection table
id = cellstr(num2str((1:numClips)'));
view = {'Spectrogram 1'};
view(1:numClips,1) = view;
chan = cellstr(num2str(ones(numClips,1)));
lowFreq = cellstr(num2str(zeros(numClips,1).*200));
% hiFreq = cellstr(num2str(ones(numClips,1).*Fs./2));
hiFreq = cellstr(num2str(ones(numClips,1).*13000));
blank = {''};
blank(1:numClips,1) = blank;

% Encode score
scoreNum = str2double(score);
scoreHex = cellstr(num2hex(scoreNum));
scoreChk = hex2num(scoreHex);
assert(all(scoreNum==scoreChk),'ERROR: Score encoding failed')

% Export as Raven selection table
headers = { ...
    'Selection', ...
    'View', ...
    'Channel', ...
    'Begin Time (s)', ...
    'End Time (s)', ...
    'Low Freq (Hz)', ...
    'High Freq (Hz)', ...
    'True/False', ...
    'Comment', ...
    'BirdNET Species', ...
    'Latitude', ...
    'Longitude', ...
    'Altitude', ...
    'Code', ...
};
if isequal(numClips,0)
    C = {};
else
    C = [ ...
        id, ...
        view, ...
        chan, ...
        beginTime, ...
        endTime, ...
        lowFreq, ...
        hiFreq, ...
        blank, ...
        blank, ...
        species, ...
        lat, ...
        lon, ...
        alt, ...
        scoreHex, ...
    ];
end
C = [headers ; C];
write_selection_table(C,fnSTfull)

% % % Export as truth table
% % headers = { ...
% %     'score', ...
% %     'id', ...
% %     'ts', ...
% %     'deviceId', ...
% %     'recordingId', ...
% % };
% % if isequal(numClips,0)
% %     C = {};
% % else
% %     C = [ ...
% %         score, ...
% %         id, ...
% %         ts, ...
% %         deviceId, ...
% %         recordingId, ...
% %     ];
% % end
% % C = [headers ; C];
% % write_selection_table(C,fnScoresFull)

% Export list of sound files to Raven listfile
write_selection_table(fnSound,fnListfileFull);
